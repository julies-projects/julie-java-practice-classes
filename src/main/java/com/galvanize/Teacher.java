package com.galvanize;

/**
 * This class is responsbile for keeping track of teahcer id, name and salary
 */

public class Teacher {
    private int id;
    private String name;
    private int salary;
    private int salaryEarned;

    /**
     * Creates a new Teacher object
     *
     * @param id     ide for the teacher
     * @param name   name of the teacher
     * @param salary for the teacher
     */
    // constructor
    public Teacher(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.salaryEarned = 0;

    }

    //return the id of the current teacher
    public int getId() {
        return id;
    }

    //return name of the teacher
    public String getName() {
        return name;
    }

    //return the salary of the teacher
    public int getSalary() {
        return salary;
    }

    //Set the salary
    public void setSalary(int salary) {
        this.salary = salary;
    }

    /**
     * Adds to the salary
     * Removes from the total money earned by the school
     *
     * @param salary
     */
    //METHOD
    public void receiveSalary(int salary) {
        salaryEarned += salary;
        School.updateTotalMoneySpent(salary);


    }

    @Override
    public String toString() {
        return "Teacher's name: " + name + " Total salary earned so far $" + salaryEarned;
    }
}


