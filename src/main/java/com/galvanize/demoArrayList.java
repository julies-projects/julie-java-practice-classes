package com.galvanize;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

public class demoArrayList {
    public static void main(String[] args) {

        //array multiple numbers - count of the numbers is defined
        //ArrayList
        List<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(32);
        numbers.add(45);
        numbers.add(76);

        System.out.println(numbers);

    }
}
