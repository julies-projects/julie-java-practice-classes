package com.galvanize;

import java.util.List;

/**
 * School can have many teachers, many students - number could cont to change - use array list
 * Implement teachers and students using an ArrayList
 *
 */
public class School {
    private List<Teacher> teachers;
    private List<Student> students;
    private static int totalMoneyEarned;
    private static int totalMoneySpent;

    /**
     * new school object is created
     * @param teachers list of teachers in the school
     * @param students list of students in the school
     */

    public School(List<Teacher> teachers, List<Student> students) {
        this.teachers = teachers;
        this.students = students;
        totalMoneyEarned=0;
        totalMoneySpent=0;
    }

    /** returns the list of teachers in the school
     *
     * @return
     */
    public List<Teacher> getTeachers() {
        return teachers;
    }

    /**
     * Adds a teacher to the school
     * @param teacher the teacher to be added
     */
    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    /**
     * return the list of students in the school
     * @return
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * add a student to the school
     * @param student student to be added
     */
    public void addStudent(Student student) {
        students.add(student);
    }

    /**
     * return the total money earned by the school.
     * @return the total money earned by the school
     */
    public int getTotalMoneyEarned() {
        return totalMoneyEarned;
    }

    /**
     * adds the total money earned by the school
     * @param MoneyEarned money that is supposed to be added
     */

    public static void updateTotalMoneyEarned(int MoneyEarned) {
        totalMoneyEarned += MoneyEarned;
    }

    /**
     * thet total money spent by the school
     * @return
     */
    public int getTotalMoneySpent() {
        return totalMoneySpent;
    }

    /**
     * update the money that is spent by the school which is the salary paid to the teachers by the school
     * @param moneySpent the money spent by school
     */
    public static void updateTotalMoneySpent(int moneySpent) {
        totalMoneyEarned-=moneySpent;
    }
}
