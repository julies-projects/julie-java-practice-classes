package com.galvanize;

// This class is responsible for keeping track of student id, fees, name, grade, fees paid.

public class Student {
    // below are fields
    private int id;
    private String name;
    private int grade;
    private int feesPaid;
    private int feesTotal;

    /**
     * Purpose of constructor is to create a new student object by initializing values
     * Fees for every student is going to be $30,000 per yr
     * Fees paid initially is 0.
     * @param id id for student: unique value
     * @param name name of the student
     * @param grade grade of the student
     */
//constructor - this is where we initialize the fields
    public Student (int id, String name, int grade) {
       // Below, we are initializing all the fields/ values this.id  from the field above = to the arg in parens
        //Fees Paid and Fees Total do not have to use this. because they are not being passed as an arg, but can still use it
        this.feesPaid=0;
        this.feesTotal=30000;
        this.id=id;
        this.name=name;
        this.grade=grade;

    }

    //Not going to alter student's name or student's id -  use set mehtod only if you're going to alter the name. We will alter grade as student is promoted to next grade
    //when we set grade we are setting the grade of student from the grade that is coming from the arg in (). we are giving permission to change the grade level. we are altering or setting the grade. setters always void, getters returning a data type. as we set grade we accept a grade from the arg int grade

    /**
     * Used to update the student's grade
     * @param grade new grade of the student
     */
    public void setGrade(int grade) {
        this.grade=grade;
    }

    /**
     * Keep adding the fees to feesPaid Field.
     * Add the fees to the fees paid.
     * The school is going to receive the funds.
     * @param fees the fees that the student pays.
     */
    public void payFees(int fees) {
        feesPaid+=fees; //this cont to add fees that are paid
        School.updateTotalMoneyEarned(feesPaid);
//        System.out.println("Fees Paid " + feesPaid);

    }
//return id of student
    public int getId () {
        return id;
    }
    //return name of student
    public String getName () {
        return name;
    }
    //return grade level of student
    public int getGrade () {
        return grade;
    }
    //return fees paid by the student
    public int getFeesPaid () {
        return feesPaid;
    }
    //return the total fees of the student
    public int getFeesTotal () {
        return feesTotal;
    }

    /**
     * return the remaining fees
     * @return
     */
    public int getRemainingFees() {
        return feesTotal-feesPaid;
            }

    @Override
    public String toString() {
        return "Student's name : "+name+ " Total fees paid so far $"+  feesPaid;
    }
}



